//
//  InputLayer.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

class InputLayer:Layer{
    
    override func settingLinks(prevLayer: Layer, nextLayer: Layer, inputCount: Int) {
        self.prevLayer = prevLayer
        self.nextLayer = nextLayer
        
        input = Array(repeating: 0.0, count: inputCount)
        w = Array(repeating: Array(repeating: 0.0, count: input.count), count: output.count)
        
        for i in 0...w.count-1{
            for j in 0...w[0].count-1{
                w[i][j] = 1
            }
        }
    }
    
    override func calculation() {
        output = input
        nextLayer.input = output
        nextLayer.calculation()
    }
    
    override func calculationError() {
        nextLayer.changeWeight()
    }
    
}

