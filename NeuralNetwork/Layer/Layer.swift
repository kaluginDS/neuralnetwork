//
//  Layer.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

class Layer{
    
    var name:String!
    
    var w:[[CGFloat]]! //[нейрон на текущем слои][нейрон на предыдущем слои]
    var input:[CGFloat]! //кол-во нейронов на предыдущем слои
    var output:[CGFloat]! //кол-во нейронов
    var error:[CGFloat]!
    
    var speed:CGFloat!
    
    var nextLayer:Layer!
    var prevLayer:Layer!
    
    var activationFunc:ActivationFunction!
    
    var isEnableDebugging = false
    
    init(){
        
    }
    
    init(_ name:String, neuronsCount:Int, activationFunc:ActivationFunction, speed:CGFloat){
        
        self.name = name
        self.speed = speed
        self.output = Array(repeating: 0.0, count: neuronsCount)
        self.error = Array(repeating: 0.0, count: neuronsCount)
        self.activationFunc = activationFunc
        
    }
    
    func settingLinks(prevLayer:Layer, nextLayer:Layer, inputCount:Int){
        self.prevLayer = prevLayer
        self.nextLayer = nextLayer
        
        input = Array(repeating: 0.0, count: inputCount)
        w = Array(repeating: Array(repeating: 0.0, count: input.count), count: output.count)
        
        for i in 0...w.count-1{
            for j in 0...w[0].count-1{
                w[i][j] = CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (0.3 - 0.1) + 0.1
                //                print("w ",w[i][j])
            }
        }
        
    }
    
    func calculation(){
        
        for i in 0...output.count-1{
            output[i] = 0
        }
        
        for i in 0...output.count-1{
            for j in 0...input.count-1{
                output[i] += input[j] * w[i][j]
            }
        }
        var OutputText = ""
        for i in 0...output.count-1{
            output[i] = activationFunc.activate(output[i])
            //            print(output[i])
            OutputText += "\(output[i]) "
        }
        
        if isEnableDebugging { print(name + #function); print(OutputText) }
        
        nextLayer.input = output
        nextLayer.calculation()
    }
    
    func calculationError(){
        
        for i in 0...error.count-1{
            error[i] = 0
        }
        
        for i in 0...nextLayer.w.count-1{
            for j in 0...nextLayer.w[0].count-1{
                error[j] += nextLayer.error[i] * nextLayer.w[i][j]
            }
        }
        
        var errorText = ""
        
        for i in 0...error.count-1{
            errorText += "\(error[i]) "
        }
        
        if isEnableDebugging { print(name + #function); print(errorText) }
        
        nextLayer.changeWeight()
        prevLayer.calculationError()
    }
    
    func changeWeight(){
        
        for i in 0...w.count-1{
            for j in 0...w[0].count-1{
                w[i][j] += error[i] * activationFunc.derivative(output[i]) * input[j] * speed
            }
        }
        
        var weightText = ""
        for i in 0...w.count-1{
            for j in 0...w[0].count-1{
                weightText += "\(w[i][j]) "
            }
        }
        
        if isEnableDebugging { print(name + #function); print(weightText) }
    }
    
}

