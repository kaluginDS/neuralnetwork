//
//  Trainer.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

struct Example{
    var input:[CGFloat]!
    var output:[CGFloat]!
}

class Trainer{
    
    var examples:[Example]!
    var network:NeuralNetwork!
    var globalError:CGFloat! //обучать до глобальной ошибки сети
    
    init(network:NeuralNetwork, examples:[Example], globalError:CGFloat ){
        self.examples = examples
        self.network = network
        self.globalError = globalError
    }
    
    func study(){
        
        var networkErr:CGFloat = 1000
        var sumErr:CGFloat = 0
        
        while networkErr > globalError {
            networkErr = 0
            sumErr = 0
            for example in examples{
                let output = network.calculationOutput(example.input)
                
                //ошибка на кадом нейроне сети
                var outputErors:[CGFloat] = Array(repeating: 0.0, count: output.count)
                
                for i in 0...output.count-1{
                    //err - ошибка на каждом слои сети
                    let err = example.output[i] - output[i]
                    //                    print(err)
                    networkErr += abs(err)
                    outputErors[i] = err
                    sumErr += abs(err)
                }
                network.calculationWeight(outputErors)
                //                print("------------------------------------------------")
            }
            
            print("МНК ", 0.5 * pow(sumErr, 2))
            print(networkErr)
            
        }
    }
}

