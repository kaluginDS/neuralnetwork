//
//  NeuralNetwork.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

class NeuralNetwork{
    
    var layers = [Layer]()
    var firstLayer:Layer!
    var lastLayer:Layer!
    var penultimateLayer:Layer!
    
    var activationFunc:ActivationFunction!
    var speed:CGFloat!
    
    var isEnableDebugging = false {didSet{
        for layer in layers{ layer.isEnableDebugging = isEnableDebugging }
        }}
    
    init(neuronsOnLayers:[Int], activationFunc:ActivationFunction, speed:CGFloat? = 0.01){
        
        self.speed = speed
        self.activationFunc = activationFunc
        
        layers.append(EmptyLayer())
        layers.append(InputLayer("Layer 0 " , neuronsCount: neuronsOnLayers[0], activationFunc:activationFunc, speed: speed!))
        
        for i in 1...neuronsOnLayers.count-1{
            //            print(neurons)
            layers.append(Layer("Layer \(i) ", neuronsCount: neuronsOnLayers[i], activationFunc:activationFunc, speed: speed!))
        }
        
        layers.append(EmptyLayer())
        
        firstLayer = layers[1]
        lastLayer = layers[layers.count-2]
        penultimateLayer = layers[layers.count-3]
        
        firstLayer.settingLinks(prevLayer: layers[0], nextLayer: layers[2], inputCount: neuronsOnLayers[0])
        for index in 2...layers.count-2{
            layers[index].settingLinks(prevLayer: layers[index-1], nextLayer: layers[index+1], inputCount: layers[index-1].output.count)
        }
        
    }
    
    func calculationOutput(_ input:[CGFloat])->[CGFloat]{
        firstLayer.input = input
        firstLayer.calculation()
        return lastLayer.output
    }
    
    func calculationWeight(_ error:[CGFloat]){
        lastLayer.error = error
        //        lastLayer.changeWeight()
        penultimateLayer.calculationError()
    }
    
    func response(on input:[CGFloat]){
        firstLayer.input = input
        firstLayer.calculation()
        for i in 0...lastLayer.output.count-1 {
            print("#\(i) - \(lastLayer.output[i])")
        }
    }
    
}

