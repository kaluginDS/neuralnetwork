//
//  main.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

var examples = [Example]()

let test = Test()
examples = test.testExamples()

//let network = NeuralNetwork(neuronsOnLayers: [15,20,20,10], activationFunc: Sigmoid(), speed: 0.1)
let network = NeuralNetwork(neuronsOnLayers: [15,20,20,10], activationFunc: Threshold(), speed: 0.01)

let trainer = Trainer(network: network, examples: examples, globalError: 5)
trainer.study()

for (index,example) in examples.enumerated(){
    print("цифра \(index):")
    network.response(on: example.input)
    print("------------------------------------------------------")
}

