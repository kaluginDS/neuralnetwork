//
//  ActivationFunction.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

protocol ActivationFunction{
    func activate(_ output:CGFloat)->CGFloat
    func derivative(_ output:CGFloat)->CGFloat
}

class Sigmoid:ActivationFunction{
    
    func activate(_ output:CGFloat)->CGFloat {
        return 1 / ( 1 + exp(-output) )
    }
    
    func derivative(_ output: CGFloat) -> CGFloat {
        return output * ( 1 - output )
    }
    
}

class Threshold: ActivationFunction {
    
    func activate(_ output:CGFloat)->CGFloat {
        if output > 0.5 {
            return 1
        }
        return 0
    }
    
    func derivative(_ output: CGFloat) -> CGFloat {
        return 1
    }
    
}

