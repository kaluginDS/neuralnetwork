//
//  Test.swift
//  NeuralNetwork
//
//  Created by Калугин on 27.07.2018.
//  Copyright © 2018 Калугин. All rights reserved.
//

import Foundation

class Test {
    
    func testExamples()->[Example] {
        var examples = [Example]()
        examples.append(Example(input: [
            1,1,1,
            1,0,1,
            1,0,1,
            1,0,1,
            1,1,1
            ], output: [1,0,0,0,0,0,0,0,0,0]))
        
        examples.append(Example(input: [
            0,1,0,
            1,1,0,
            0,1,0,
            0,1,0,
            1,1,1
            ], output: [0,1,0,0,0,0,0,0,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            0,0,1,
            1,1,1,
            1,0,0,
            1,1,1
            ], output: [0,0,1,0,0,0,0,0,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            0,0,1,
            1,1,1,
            0,0,1,
            1,1,1
            ], output: [0,0,0,1,0,0,0,0,0,0]))
        
        examples.append(Example(input: [
            1,0,1,
            1,0,1,
            1,1,1,
            0,0,1,
            0,0,1
            ], output: [0,0,0,0,1,0,0,0,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            1,0,0,
            1,1,1,
            0,0,1,
            1,1,1
            ], output: [0,0,0,0,0,1,0,0,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            1,0,0,
            1,1,1,
            1,0,1,
            1,1,1
            ], output: [0,0,0,0,0,0,1,0,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            0,0,1,
            0,1,0,
            1,1,1,
            0,1,0
            ], output: [0,0,0,0,0,0,0,1,0,0]))
        
        examples.append(Example(input: [
            1,1,1,
            1,0,1,
            1,1,1,
            1,0,1,
            1,1,1
            ], output: [0,0,0,0,0,0,0,0,1,0]))
        
        examples.append(Example(input: [
            1,1,1,
            1,0,1,
            1,1,1,
            0,0,1,
            1,1,1
            ], output: [0,0,0,0,0,0,0,0,0,1]))
        return examples
    }
    
}

